import re

import unidecode
from langdetect import detect, LangDetectException

patterns = ['ins', 'ig', 'insta', 'instagram']


def remove_links(x):
    p = re.sub("(?P<url>https?://[^\s]+)", "", x)
    return p


def remove_accent(text):
    return unidecode.unidecode(text)


def remove_emojis(data):
    emoj = re.compile("["
                      u"\U0001F600-\U0001F64F"  # emoticons
                      u"\U0001F300-\U0001F5FF"  # symbols & pictographs
                      u"\U0001F680-\U0001F6FF"  # transport & map symbols
                      u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                      u"\U00002500-\U00002BEF"  # chinese char
                      u"\U00002702-\U000027B0"
                      u"\U00002702-\U000027B0"
                      u"\U000024C2-\U0001F251"
                      u"\U0001f926-\U0001f937"
                      u"\U00010000-\U0010ffff"
                      u"\u2640-\u2642"
                      u"\u2600-\u2B55"
                      u"\u200d"
                      u"\u23cf"
                      u"\u23e9"
                      u"\u231a"
                      u"\ufe0f"  # dingbats
                      u"\u3030"
                      "]+", re.UNICODE)
    return re.sub(emoj, ' ', data)


def remove_punc_string(string):
    punc = '''!()-+[]{};':",/\<>?@#$%^&*~'''
    for ele in string:
        if ele in punc:
            string = string.replace(ele, "")
    yield string


def find_insta(item):
    item = remove_emojis(item)
    ig = re.search('instagram.com/', item.lower())
    nickname = None
    if ig is not None:
        a = item.partition("instagram.com/")[-1]
        if 1 < len(a.partition('/')[0]) < 31:
            nickname = a.partition('/')[0].lstrip()
    else:
        for pattern in patterns:
            ins = re.search(pattern, item.lower())
            item = remove_links(item)
            if ins is not None:
                if 1 < len(item.partition(":")[-1]) < 30:
                    if ' ' not in item.partition(":")[-1].lstrip():
                        try:
                            t = detect(item.partition(":")[-1].lstrip())
                            if t != 'error':
                                a = next(remove_punc_string(item.partition(":")[-1]))
                                if 'www.' not in a:
                                    nickname = a.lstrip()
                        except LangDetectException:
                            pass
                elif len(item.partition(":")[-1]) > 30:
                    for word in item.split(':'):
                        a = next(remove_punc_string(word))
                        try:
                            for i in a.split(' '):
                                if i.rstrip('.').isalpha() is False and len(i) > 1:
                                    t = detect(i.rstrip('.'))
                                    if t != 'vi' and t != 'en':
                                        if '.com' not in i.lower() and 'www.' not in i.lower():
                                            nickname = i.rstrip('.').lstrip()
                        except LangDetectException:
                            pass
    return nickname
