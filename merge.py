import itertools
import json

import pika

from utils import parameters

list_phone = []
list_link = []
list_stk = []
list_bank = []
list_insta = []
list_zalo = []
list_viber = []


def combine_list(list1, list2):
    new_list = list(set(itertools.chain(list1, list2))) if list2 else list(set(list1))
    return new_list


def merge_info(dataline):
    page_id = dataline.get('Page ID', None)
    p = [i for i, d in enumerate(list_post) if page_id in d]
    if len(p) > 0:
        phone_about = dataline.get('Phone', None)
        link_about = dataline.get('Link In About', None)
        stk_about = dataline.get('STK', None)
        bank_about = dataline.get('Bank', None)
        insta_about = dataline.get('Insta', None)
        viber_about = dataline.get('Viber', None)
        zalo_about = dataline.get('Zalo', None)
        for i in p:
            link_post = list_post[i][page_id]['Link In Post']
            phone_post = list_post[i][page_id]['Phone']
            stk_post = list_post[i][page_id]['STK']
            bank_post = list_post[i][page_id]['Bank']
            insta_post = list_post[i][page_id]['Insta']
            zalo_post = list_post[i][page_id]['Zalo']
            viber_post = list_post[i][page_id]['Viber']
            if link_post:
                list_link.extend(link_post)
            if phone_post:
                list_phone.extend(phone_post)
            if stk_post:
                list_stk.extend(stk_post)
            if bank_post:
                list_bank.extend(bank_post)
            if insta_post:
                list_insta.append(insta_post)
            if zalo_post:
                list_zalo.extend(zalo_post)
            if viber_post:
                list_viber.extend(viber_post)
        phones = combine_list(list_phone, phone_about)
        links = combine_list(list_link, link_about)
        stks = combine_list(list_stk, stk_about)
        banks = combine_list(list_bank, bank_about)
        instas = combine_list(list_insta, insta_about)
        zalos = combine_list(list_zalo, zalo_about)
        vibers = combine_list(list_viber, viber_about)
        if len(links) > 0:
            dataline['Links'] = links
        if len(phones) > 0:
            dataline['Phone'] = phones
        if len(stks) > 0:
            dataline['STK'] = stks
        if len(banks) > 0:
            dataline['Bank'] = banks
        if len(instas) > 0:
            dataline['Insta'] = instas
        if len(zalos) > 0:
            dataline['Zalo'] = zalos
        if len(vibers) > 0:
            dataline['Viber'] = vibers
        list_phone.clear()
        list_link.clear()
        list_bank.clear()
        list_stk.clear()
        list_insta.clear()
        list_zalo.clear()
        list_viber.clear()
    return dataline


# def start_pool(pool, pages):
#
#     for page in pool.imap_unordered(extract_page, total_page):
#         channel.basic_publish(exchange='', routing_key='page_info', body=json.dumps(page, ensure_ascii=False))

if __name__ == '__main__':
    list_key = []
    list_page = []
    count = 0
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    for method_frame, properties, body in channel.consume('post_info'):
        raw_post = body.decode()
        if raw_post == 'end_post':
            channel.basic_ack(method_frame.delivery_tag)
            break
        else:
            list_key.append(raw_post)
            channel.basic_ack(method_frame.delivery_tag)
    channel.close()
    list_key = list(set(list_key))
    list_post = [json.loads(i) for i in list_key]
    channel = connection.channel()
    for method, propers, message in channel.consume('page_info'):
        raw_page = message.decode()
        if raw_page == 'end_post':
            channel.basic_ack(method.delivery_tag)
            break
        else:
            page = json.loads(raw_page)
            list_page.append(page)
            channel.basic_ack(method.delivery_tag)
    for line in list_page:
        count += 1
        print(count)
        t = merge_info(line)
        channel.basic_publish(exchange='', routing_key='final_page_info', body=json.dumps(t, ensure_ascii=False))
        if t['Phone']:
            channel.basic_publish(exchange='', routing_key='sdt', body=json.dumps(t, ensure_ascii=False))
    end = "end_post"
    channel.basic_publish(exchange='', routing_key='final_page_info', body=end)
    channel.basic_publish(exchange='', routing_key='sdt', body=end)
    channel.close()
    connection.close()
