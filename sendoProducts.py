import scrapy
import json
import pandas as pd
from datetime import datetime
import os

apis = []
page = []

class crawlReviewsShopee(scrapy.Spider):
    path = '/home/intern/anhmyphuong/shopee/crawlShopee/crawlShopee/spiders/product_list_sendo.csv'
    username_file = open(path, 'r', encoding='utf-8')
    username_data = pd.read_csv(username_file)
    usernames = list(username_data['shopid'])
    num_product = list(username_data['num_product'])

    for i in range(len(num_product)):
        num_page = int(num_product[i] / 60) + 1
        page.append(num_page)

    for i in range(len(usernames)):
        for j in range(1, page[i] + 1):
            api = 'https://searchlist-api.sendo.vn/web/products?admin_id=' + str(usernames[i]) + '&page=' + str(
                j) + '&platform=2&search_algo=algo6&seller_admin_id=' + str(usernames[i]) + '&size=60&sortType=rank'
            apis.append(api)

    def start_requests(self):
        headers = {
            "referer": "https://www.sendo.vn/",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36",
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "en-US,en;q=0.9,vi-VN;q=0.8,vi;q=0.7,ja-JP;q=0.6,ja;q=0.5",
        }
        for api in apis:
            yield scrapy.http.Request(api, headers=headers)

    name = 'sendoProduct'
    allowed_domains = ['sendo.vn']

    def parse(self, response):
        resp = json.loads(response.body)
        info = resp.get('data')

        for data in info:
            shop_info = data.get('shop')
            price_average = (data.get('sale_price_min') + data.get('sale_price_max')) / 2
            quantity = data.get('sold')
            yield {
                'shopid': shop_info.get('id'),
                "shop_location": shop_info.get('ware_house_region_name'),
                'itemid': data.get('id'),
                'product_name': data.get('name'),
                'price_min': data.get('sale_price_min'),
                'price_max': data.get('sale_price_max'),
                'price_average': price_average,
                'quantity': quantity,
                'revenue': price_average * quantity
            }
