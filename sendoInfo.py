import scrapy
import json
import pandas as pd
from datetime import datetime
import os

apis = []
page = []

class crawlReviewsShopee(scrapy.Spider):
    path = '/home/intern/anhmyphuong/shopee/crawlShopee/crawlShopee/spiders/sendo_final_id.csv'
    username_file = open(path, 'r', encoding='utf-8')
    username_data = pd.read_csv(username_file)
    usernames = list(username_data['shopid'])

    for i in range(len(usernames)):
        api = 'https://searchlist-api.sendo.vn/web/products?admin_id=' + str(usernames[i]) + '&page=1&platform=2&search_algo=algo6&seller_admin_id=' + str(usernames[i]) + '&size=60&sortType=rank'
        apis.append(api)

    def start_requests(self):
        headers = {
            "referer": "https://www.sendo.vn/",
            "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36",
            "accept-encoding": "gzip, deflate, br",
            "accept-language": "en-US,en;q=0.9,vi-VN;q=0.8,vi;q=0.7,ja-JP;q=0.6,ja;q=0.5",
        }
        for api in apis:
            yield scrapy.http.Request(api, headers=headers)

    name = 'sendoProduct'
    allowed_domains = ['sendo.vn']
    def parse(self, response):
        resp = json.loads(response.body)
        data = resp.get('data')
        meta_data = resp.get('meta_data')

        #for data in info:
        shop_info = data[0].get('shop')
        yield {
                'shopid': shop_info.get('id'),
                'shop_name': shop_info.get('name'),
                "shop_location": shop_info.get('ware_house_region_name'),
                'num_product': meta_data.get('total_count'),
           }

