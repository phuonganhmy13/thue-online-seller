import re

from modules.instagram import find_insta
from modules.number import find_phone, find_bank, find_zalo, find_bank_account, find_viber


def find_links(text):
    links = re.findall("(?P<url>https?://[^\s]+)", text)
    if len(links) == 0:
        return None
    return links


def remove_punc_number(string):
    number = []
    for ele in string:
        ele = ele.replace(" ", "")
        if '.' in ele:
            ele = ele.replace(ele, "")
        if ele.startswith("+84"):
            ele = ele.replace("+84", "0")
        if ele.startswith("84"):
            ele = ele.replace("84", "0")
        if not ele.startswith("0") and len(ele) > 6:
            ele = "0" + ele
        number.append(ele)
    return number


def flatten(t):
    return [item for sublist in t for item in sublist]


def run_all(data):
    instas = [find_insta(item) for item in data.split("\n")]
    phone_about = flatten([find_phone(item) for item in data.split("\n") if find_phone(item)])
    zalos = flatten([find_zalo(item) for item in data.split("\n") if find_zalo(item)])
    vibers = flatten([find_viber(item) for item in data.split("\n") if find_viber(item)])
    bank_accounts = flatten([find_bank_account(item) for item in data.split("\n") if find_bank_account(item)])
    bank = None
    bank_account = None
    if bank_accounts:
        bank_account = bank_accounts
        if len(list(set(find_bank(data)))) > 0:
            bank = list(set(find_bank(data)))
    return phone_about, bank_account, bank, zalos, vibers, instas
