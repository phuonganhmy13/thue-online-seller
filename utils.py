from datetime import datetime

def _datetime(date_str):
    if date_str != date_str:
        return 0
    return datetime.strptime(date_str, '%Y-%m-%d')


def year_difference(df, year_joined):
    now = datetime.now().strftime('%Y-%m-%d')
    for time in df['joined_date']:
        currentTime = _datetime(now)
        joinedTime = _datetime(time)
        if joinedTime == 0:
            year_joined.append(0)
        else:
            delta = (currentTime - joinedTime)
            year_joined.append("%.2f" % (delta.total_seconds() / 31536000))
    return year_joined

def cal_average_revenue(df, average_revenue):
    for i in range(len(df["years_has_joined"])):
        if df["years_has_joined"][i] == 0:
            avg = float("NaN")

        elif float(df["years_has_joined"][i]) >= 1.00:
            avg = (df["total_revenue"][i] / float(df["years_has_joined"][i]))
        else:
            avg = df["total_revenue"][i]
        average_revenue.append(float("%.0f" % avg))
    return average_revenue

def cal_total(df, total_revenue):
    for i in range(len(df["years_has_joined"])):
        if df["years_has_joined"][i] == 0:
            avg = float("NaN")

        elif float(df["years_has_joined"][i]) >= 1.00:
            avg = (df["average_revenue"][i] * float(df["years_has_joined"][i])) * 12
        else:
            avg = df["average_revenue"][i]
        total_revenue.append(float("%.0f" % avg))
    return total_revenue

import multiprocessing
import re

import pika

from modules.instagram import find_insta
from modules.number import find_phone, find_bank, find_zalo, find_bank_account, find_viber

cpu = multiprocessing.cpu_count() - 1
credentials = pika.PlainCredentials('guest', 'guest')
parameters = pika.ConnectionParameters('localhost',
                                       5672,
                                       '/',
                                       credentials)


def find_links(text):
    links = re.findall("(?P<url>https?://[^\s]+)", text)
    if len(links) == 0:
        return None
    return links


def remove_punc_number(string):
    number = []
    for ele in string:
        ele = ele.replace(" ", "")
        if '.' in ele:
            ele = ele.replace(ele, "")
        if ele.startswith("+84"):
            ele = ele.replace("+84", "0")
        if ele.startswith("84"):
            ele = ele.replace("84", "0")
        if not ele.startswith("0") and len(ele) > 6:
            ele = "0" + ele
        number.append(ele)
    return number


def flatten(t):
    return [item for sublist in t for item in sublist]


def run_all(data):
    instas = [find_insta(item) for item in data.split("\n")]
    phone_about = flatten([find_phone(item) for item in data.split("\n") if find_phone(item)])
    zalos = flatten([find_zalo(item) for item in data.split("\n") if find_zalo(item)])
    vibers = flatten([find_viber(item) for item in data.split("\n") if find_viber(item)])
    bank_accounts = flatten([find_bank_account(item) for item in data.split("\n") if find_bank_account(item)])
    bank = None
    bank_account = None
    if bank_accounts:
        bank_account = bank_accounts
        if len(list(set(find_bank(data)))) > 0:
            bank = list(set(find_bank(data)))
    return phone_about, bank_account, bank, zalos, vibers, instas
