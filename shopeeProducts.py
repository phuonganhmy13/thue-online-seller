import scrapy
import json
import pandas as pd
import os

class crawlReviewsShopee(scrapy.Spider):
    path = 'C:/Users/MSI 14RAS/PycharmProjects/shopee/crawlShopee/es_info.csv'
    username_file = open(path, 'r', encoding='utf-8')
    data = pd.read_csv(path, engine='python')

    idShop = (list(data['Shopee_ID']))
    num_product = (list(data['num_product']))

    apis = []
    for i in range(len(idShop)):
        for count in range(0, int(num_product[i]), 30):
            api = "https://shopee.vn/api/v4/search/search_items?by=pop&limit=30&match_id=" + str(
                idShop[i]) + "&newest=" + str(
                count) + "&only_soldout=1&order=desc&page_type=shop&scenario=PAGE_OTHERS&version=2"
            apis.append(api)
    for i in range(len(idShop)):
        for count in range(0, int(num_product[i]), 30):
            api = "https://shopee.vn/api/v4/search/search_items?by=pop&limit=30&match_id=" + str(
                idShop[i]) + "&newest=" + str(count) + "&order=desc&page_type=shop&scenario=PAGE_OTHERS&version=2"
            apis.append(api)
            'https://shopee.vn/api/v4/search/search_items?by=pop&limit=30&match_id='+'&newest='+'&order=desc&page_type=shop&scenario=PAGE_OTHERS&version=2'

    name = 'products'
    allowed_domains = ['shopee.vn']
    start_urls = apis

    def parse(self, response):
        api_url = str(response.request.url)
        global status
        if "only_soldout" in api_url:
            status = "Hết hàng"
        else:
            status = "Còn hàng"

        resp = json.loads(response.body)
        data = resp.get('items')
        for d in data:
            info = d.get('item_basic')
            price_average = (info.get('price') + info.get('price_max')) / 200000
            quantity = info.get('historical_sold')
            yield {
                'shopid': info.get('shopid'),
                'itemid': info.get('itemid'),
                'product_name': info.get('name'),
                'status': status,
                "shop_location": info.get('shop_location'),
                'price_min': info.get('price'),
                'price_max': info.get('price_max'),
                'price_average': price_average,
                'quantity': quantity,
                'revenue': price_average * quantity,
            }
