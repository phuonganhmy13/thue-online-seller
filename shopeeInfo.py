import scrapy
import json
from datetime import datetime
import pandas as pd

class crawlShopDetail(scrapy.Spider):
    path = 'C:/Users/MSI 14RAS/PycharmProjects/shopee/crawlShopee/es_mapping.csv'
    username_file = open(path, 'r', encoding='utf-8')
    username_data = pd.read_csv(username_file)
    usernames = (list(username_data['Shopee_ID']))
    apis = []

    for i in range(len(usernames)):
        api = "https://shopee.vn/api/v4/shop/get_shop_detail?username=" + str(usernames[i])
        apis.append(api)
    print(apis)
    name = 'shopInfo'
    allowed_domains = ['shopee.vn']
    start_urls = apis

    def parse(self, response):
        res = json.loads(response.body)
        data = res.get('data')
        joined_time = datetime.fromtimestamp(data.get('ctime')).strftime('%Y-%m-%d')
        num_product = data.get('item_count')
        yield {
            "Shopee_ID": data.get('shopid'),
            "username": data.get('account').get('username'),
            "shop_name": data.get('name'),
            "shop_location": data.get('shop_location'),
            "joined_date": joined_time,
            "num_product": num_product
        }

